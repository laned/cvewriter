from distutils.core import setup

setup(
    name='CveWriter',
    version='1.0.0',
    author='Darryl lane',
    author_email='DarrylLane101@gmail.com',
    packages=['CveWriter'],
    url='https://bitbucket.org/laned/cvewriter/',
    license='LICENSE.txt',
    description='Take CVE list > HTML output',
    long_description=open('README.txt').read(),
    scripts=['CveWriter/CveWriter.py'],
    install_requires=[
        "elementtree",
        "docopt",
    ],
)
