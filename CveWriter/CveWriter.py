#!/usr/local/bin/python

"""
Description:

This script takes a nessus file containing all vulnerabilities that require scraping for CVE's and writes all output to an HTML page that contains href links.

Authors: Darryl Lane

Usage:
  CveWriter.py (-c <nessus_file>) (-f <file_name>)
  CveWriter.py -h | --help
  CveWriter.py --version

Options:
  -c                 Nessus file containing only items of interest.
  -f                 The name of the file to save the html output
  -h --help          Show this screen.
  --version          Show version.
  """


def writeFile(cve_list, file_name):
#creates output file of all CVEs and associated links
	try:
		with open(file_name, "a") as f:
			url_list = []
			f.write("<html><body><table>")
			for item in cve_list:
				data = "<a href=\"https://web.nvd.nist.gov/view/vuln/detail?vulnId={}\">{}</a>" .format (item, item)
				url_list.append(data)
			new_string = (', '.join(url_list))
			f.write("<tr><td>{}</td></tr>".format (new_string))
			f.write("</table></body></html>")
			f.close
	except Exception as e:
		print e
		exit()

def getCVE(nessus_file):
#Takes a Nessus input file and scrapes all CVEs from the document
	cve_list = []
	try:
		tree = ET.parse(nessus_file)
		doc = tree.getroot()
		walk = doc.getiterator('cve')
		for cve in walk:
			cve_list.append(cve.text)

		cve_list = sorted(set(cve_list))
		return cve_list
	except Exception as e:
		print e
		exit()

if __name__ == "__main__":
		from docopt import docopt
		import elementtree.ElementTree as ET
		import os
		arguments = docopt(__doc__, version= '1.0.0')

		cve_list = getCVE(arguments['<nessus_file>'])
		writeFile(cve_list, arguments['<file_name>'])